﻿using System;
using System.Collections.Generic;
using System.Linq;
using Checkout.Domain.Interfaces;

namespace Checkout.Domain
{
    public class Basket : IBasket
    {
        private IList<IItem> _items = new List<IItem>();
        
        public Basket()
        {
            
        }
        public void Scan(IItem item)
        {
            if(item == null)
                throw new ArgumentNullException("item cannot be null",nameof(IItem));
            _items.Add(item);
        }

        public decimal GetTotalPrice()
        {

            decimal defaultOfferPrice = 0;
            var basketTotal = _items.Sum(x => x.UnitPrice);



            var distinctItemSkus = _items.Where(i => i.Offer != null).Select(i => i.Sku).Distinct();

            foreach (var sku in distinctItemSkus)
            {
                var offer = _items.FirstOrDefault(x => x.Sku == sku)?.Offer;

                var totalSameItems = _items.Where(x => x.Sku == sku);

                var discountedPrice = CalculateDiscount(totalSameItems.Count(), offer);
                if (discountedPrice == defaultOfferPrice) continue;
                {
                    var total = totalSameItems.Sum(x => x.UnitPrice);

                    var priceToDeduct = (decimal)(total - discountedPrice);

                    basketTotal -= priceToDeduct;
                }


            }


            return basketTotal;

        }


        private decimal CalculateDiscount(int itemCount, IOffer offer)
        {
            // ReSharper disable once PossibleInvalidOperationException
            return (decimal) ((itemCount / offer?.Quantity) * offer?.OfferPrice);
        }


    }
}
