﻿using Checkout.Domain.Interfaces;

namespace Checkout.Domain
{
    public class Offer :IOffer
    {
        public string Sku { get;  }
        public int Quantity { get;  }
        public decimal OfferPrice { get; }

        public Offer(string sku, int quantity, decimal offerPrice)
        {
            Sku = sku;
            Quantity = quantity;
            OfferPrice = offerPrice;
        }

        

    }
}
