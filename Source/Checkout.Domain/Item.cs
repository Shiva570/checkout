﻿using System;
using Checkout.Domain.Interfaces;

namespace Checkout.Domain
{
    public class Item : IItem
    {
        public string Sku { get; set; }
        public decimal	 UnitPrice { get; set; }
        public IOffer Offer { get; set; }
    }
}
