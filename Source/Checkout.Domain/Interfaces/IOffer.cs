﻿namespace Checkout.Domain.Interfaces
{
    public interface IOffer
    {

        string Sku { get; }

        int Quantity
        {
            get;  

        }

          decimal OfferPrice { get; }
         
    }
}
