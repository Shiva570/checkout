﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Checkout.Domain.Interfaces
{
    public interface IBasket
    {
        void Scan(IItem item);
        decimal GetTotalPrice();
    }
}
