﻿namespace Checkout.Domain.Interfaces
{
    public interface IItem
    {
          string Sku { get; set; }
          decimal UnitPrice { get; set; }
          IOffer Offer { get; set; }
    }

}
