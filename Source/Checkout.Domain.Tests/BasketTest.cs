using System;
using Checkout.Domain.Interfaces;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace Checkout.Domain.Tests
{
    [TestFixture]
    public class BasketTests
    {
        private IBasket _basket;

        [SetUp]
        public void init()
        {
            _basket = new Basket();
        }



        [Test]
        public void If_Given_Item_Then_Scan_Should_Accept_Item() 
        {
            //Arrange
            IItem item = new Item();
          
          
            //Assert

            Assert.DoesNotThrow(()=>_basket.Scan(item));
        }

        [Test]
        public void If_Given_Item_has_Valid_Sku_and_Price_Then_Scan_Should_Accept_Item()
        {

            //Arrange
            IItem item = new Item
            {
                Sku ="A99",
                UnitPrice = (decimal) 0.50
            };
            
            //Act

            //Assert

            Assert.DoesNotThrow(() => _basket.Scan(item));
        }

        [Test]
        public void If_Given_Null_Item_Then_Scan_Should_Through_Argument_Null_Exception()
        {

            //Arrange
            IItem  item= null;
            
            //Act

            //Assert

            Assert.Throws(typeof(ArgumentNullException),()=> _basket.Scan(item));
        }

        [Test]
        public void If_No_Items_Are_Scanned_Then_Basket_Should_Return_Total_As_Zero()
        {
            //Arrange
    
           
            var expectedTotal = default(decimal);
            //Act
            var actualTotal = _basket.GetTotalPrice();
            //Assert
            Assert.That(actualTotal,Is.EqualTo(expectedTotal));


        }


        [Test]
        public void If_Single_Item_Scanned_Then_Basket_Should_Return_Total_As_Item_Unit_Price()
        {

            //Arrange
            IItem item = new Item
            {
                Sku = "A99",
                UnitPrice = (decimal)0.50
            };
         
            var expectedTotal = (decimal) 0.50;

            _basket.Scan(item);
            //Act
            var actualTotal = _basket.GetTotalPrice();
            //Assert
            Assert.That(actualTotal, Is.EqualTo(expectedTotal));
        }


        [Test]
        public void If_Two_different_Items_Scanned_Then_Basket_Should_Return_Total()
        {

            //Arrange
            IItem item1 = new Item
            {
                Sku = "A99",
                UnitPrice = (decimal)0.50
            };
            IItem item2 = new Item
            {
                Sku = "B15",
                UnitPrice = (decimal)0.30
            };
            
            var expectedTotal = (decimal)0.80;

            _basket.Scan(item1);
            _basket.Scan(item2);
            //Act
            var actualTotal = _basket.GetTotalPrice();
            //Assert
            Assert.That(actualTotal, Is.EqualTo(expectedTotal));
        }


        [Test]
        public void If_Two_Biscuits_Items_Scanned_Then_Basket_Should_Return_Discounted_Price()
        {

            //Arrange
            var offer = new Offer("B15", 2, (decimal) 0.45);
            IItem item1 = new Item
            {
                Sku = "B15",
                UnitPrice = (decimal)0.30,
                Offer = offer
            };
            IItem item2 = new Item
            {
                Sku = "B15",
                UnitPrice = (decimal)0.30,
                Offer = offer
            };

            var expectedTotal = (decimal)0.45;

            _basket.Scan(item1);
            _basket.Scan(item2);
            //Act
            var actualTotal = _basket.GetTotalPrice();
            //Assert
            Assert.That(actualTotal, Is.EqualTo(expectedTotal));
        }


        [Test]
        public void If_Two_Biscuits_And_One_Apple_Items_Scanned_Then_Basket_Should_Return_Discounted_Price()
        {

            //Arrange
            var biscuitsOffer = new Offer("B15", 2, (decimal)0.45);
            var appleOffer = new Offer("A99",3,(decimal)1.30);
            IItem item1 = new Item
            {
                Sku = "B15",
                UnitPrice = (decimal)0.30,
                Offer = biscuitsOffer
            };
            IItem item2 = new Item
            {
                Sku = "A99",
                UnitPrice = (decimal)0.50,
                Offer = appleOffer
            };

            IItem item3 = new Item
            {
                Sku = "B15",
                UnitPrice = (decimal)0.30,
                Offer = biscuitsOffer
            };

            var expectedTotal = (decimal)0.95;

            _basket.Scan(item1);
            _basket.Scan(item2);
            _basket.Scan(item3);
            //Act
            var actualTotal = _basket.GetTotalPrice();
            //Assert
            Assert.That(actualTotal, Is.EqualTo(expectedTotal));
        }


        [Test]
        public void If_Valid_Items_Scanned_Then_Basket_Should_Return_Discounted_Price()
        {

            //Arrange
            var biscuitsOffer = new Offer("B15", 2, (decimal)0.45);
            var appleOffer = new Offer("A99", 3, (decimal)1.30);
            IItem item1 = new Item
            {
                Sku = "B15",
                UnitPrice = (decimal)0.30,
                Offer = biscuitsOffer
            };
            IItem item2 = new Item
            {
                Sku = "A99",
                UnitPrice = (decimal)0.50,
                Offer = appleOffer
            };

            IItem item3 = new Item
            {
                Sku = "C40",
                UnitPrice = (decimal)0.60
            };

            var expectedTotal = (decimal)1.40;

            _basket.Scan(item1);
            _basket.Scan(item2);
            _basket.Scan(item3);
            //Act
            var actualTotal = _basket.GetTotalPrice();
            //Assert
            Assert.That(actualTotal, Is.EqualTo(expectedTotal));
        }


        [Test]
        public void If_Three_Apples_Two_Biscuits_Items_Scanned_Then_Basket_Should_Return_Discounted_Price()
        {

            //Arrange
            var biscuitsOffer = new Offer("B15", 2, (decimal)0.45);
            var appleOffer = new Offer("A99", 3, (decimal)1.30);
            IItem item1 = new Item
            {
                Sku = "B15",
                UnitPrice = (decimal)0.30,
                Offer = biscuitsOffer
            };
            IItem item2 = new Item
            {
                Sku = "A99",
                UnitPrice = (decimal)0.50,
                Offer = appleOffer
            };
            IItem item3 = new Item
            {
                Sku = "A99",
                UnitPrice = (decimal)0.50,
                Offer = appleOffer
            };
            IItem item4 = new Item
            {
                Sku = "A99",
                UnitPrice = (decimal)0.50,
                Offer = appleOffer
            };

          

            var expectedTotal = (decimal)1.60;

            _basket.Scan(item1);
            _basket.Scan(item2);
            _basket.Scan(item3);
            _basket.Scan(item4);
            //Act
            var actualTotal = _basket.GetTotalPrice();
            //Assert
            Assert.That(actualTotal, Is.EqualTo(expectedTotal));
        }


    }


}